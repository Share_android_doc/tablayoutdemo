package com.example.articlemanagment.entity;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ArticleRespone {

    @SerializedName("PAGINATION")
    private PAGINATIONEntity PAGINATION;
    @SerializedName("DATA")
    private List<DATAEntity> DATA;
    @SerializedName("MESSAGE")
    private String MESSAGE;
    @SerializedName("CODE")
    private String CODE;

    public PAGINATIONEntity getPAGINATION() {
        return PAGINATION;
    }

    public void setPAGINATION(PAGINATIONEntity PAGINATION) {
        this.PAGINATION = PAGINATION;
    }

    public List<DATAEntity> getDATA() {
        return DATA;
    }

    public void setDATA(List<DATAEntity> DATA) {
        this.DATA = DATA;
    }

    public String getMESSAGE() {
        return MESSAGE;
    }

    public void setMESSAGE(String MESSAGE) {
        this.MESSAGE = MESSAGE;
    }

    public String getCODE() {
        return CODE;
    }

    public void setCODE(String CODE) {
        this.CODE = CODE;
    }

    @Override
    public String toString() {
        return "ArticleRespone{" +
                "PAGINATION=" + PAGINATION +
                ", DATA=" + DATA +
                ", MESSAGE='" + MESSAGE + '\'' +
                ", CODE='" + CODE + '\'' +
                '}';
    }
}
