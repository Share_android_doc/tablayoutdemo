package com.example.articlemanagment.entity;

import com.google.gson.annotations.SerializedName;

public class DATAEntity {
    @SerializedName("IMAGE")
    private String IMAGE;
    @SerializedName("CATEGORY")
    private CATEGORYEntity CATEGORY;
    @SerializedName("STATUS")
    private String STATUS;
    @SerializedName("AUTHOR")
    private AUTHOREntity AUTHOR;
    @SerializedName("CREATED_DATE")
    private String CREATED_DATE;
    @SerializedName("DESCRIPTION")
    private String DESCRIPTION;
    @SerializedName("TITLE")
    private String TITLE;
    @SerializedName("ID")
    private int ID;

    public String getIMAGE() {
        return IMAGE;
    }

    public void setIMAGE(String IMAGE) {
        this.IMAGE = IMAGE;
    }

    public CATEGORYEntity getCATEGORY() {
        return CATEGORY;
    }

    public void setCATEGORY(CATEGORYEntity CATEGORY) {
        this.CATEGORY = CATEGORY;
    }

    public String getSTATUS() {
        return STATUS;
    }

    public void setSTATUS(String STATUS) {
        this.STATUS = STATUS;
    }

    public AUTHOREntity getAUTHOR() {
        return AUTHOR;
    }

    public void setAUTHOR(AUTHOREntity AUTHOR) {
        this.AUTHOR = AUTHOR;
    }

    public String getCREATED_DATE() {
        return CREATED_DATE;
    }

    public void setCREATED_DATE(String CREATED_DATE) {
        this.CREATED_DATE = CREATED_DATE;
    }

    public String getDESCRIPTION() {
        return DESCRIPTION;
    }

    public void setDESCRIPTION(String DESCRIPTION) {
        this.DESCRIPTION = DESCRIPTION;
    }

    public String getTITLE() {
        return TITLE;
    }

    public void setTITLE(String TITLE) {
        this.TITLE = TITLE;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    @Override
    public String toString() {
        return "DATAEntity{" +
                "IMAGE='" + IMAGE + '\'' +
                ", CATEGORY=" + CATEGORY +
                ", STATUS='" + STATUS + '\'' +
                ", AUTHOR=" + AUTHOR +
                ", CREATED_DATE='" + CREATED_DATE + '\'' +
                ", DESCRIPTION='" + DESCRIPTION + '\'' +
                ", TITLE='" + TITLE + '\'' +
                ", ID=" + ID +
                '}';
    }
}
