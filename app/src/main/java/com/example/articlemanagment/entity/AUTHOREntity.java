package com.example.articlemanagment.entity;

import com.google.gson.annotations.SerializedName;

public class AUTHOREntity {
    @SerializedName("ID")
    private int ID;

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }
}
