package com.example.articlemanagment.entity;

import com.google.gson.annotations.SerializedName;

public class CATEGORYEntity {
    @SerializedName("NAME")
    private String NAME;
    @SerializedName("ID")
    private int ID;

    public String getNAME() {
        return NAME;
    }

    public void setNAME(String NAME) {
        this.NAME = NAME;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }
}
