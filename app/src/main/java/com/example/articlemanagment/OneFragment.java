package com.example.articlemanagment;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.example.articlemanagment.adapter.ArticleAdapter;
import com.example.articlemanagment.api.network.ServiceGenerator;
import com.example.articlemanagment.api.service.ArticleService;
import com.example.articlemanagment.callBack.ArticleCallBack;
import com.example.articlemanagment.entity.ArticleRespone;
import com.example.articlemanagment.entity.DATAEntity;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;


/**
 * A simple {@link Fragment} subclass.
 */
public class OneFragment extends Fragment {

     ArticleCallBack articleCallBack;
     RecyclerView recyclerView;
     ArticleAdapter articleAdapter;
     List<DATAEntity> dataEntityList;
     CompositeDisposable disposable = new CompositeDisposable();
     ArticleService articleService;
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
       // articleCallBack = (ArticleCallBack) context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_one, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        recyclerView = view.findViewById(R.id.rvView);
        articleService = ServiceGenerator.createService(ArticleService.class);
        dataEntityList = new ArrayList<>();
        getArticle();



    }

    void getArticle(){

        disposable.add(
                articleService.getAllArticles()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribeWith(new DisposableObserver<ArticleRespone>(){

                    @Override
                    public void onNext(ArticleRespone articleRespone) {

                       for (DATAEntity dataEntity : articleRespone.getDATA()){
                           dataEntityList.add(dataEntity);
                       }


                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e("ooo", e.toString());
                    }

                    @Override
                    public void onComplete() {

                        articleAdapter = new ArticleAdapter(dataEntityList, getContext().getApplicationContext());
                        recyclerView.setLayoutManager(new LinearLayoutManager(getContext().getApplicationContext(), LinearLayoutManager.VERTICAL,false));
                        recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL));
                        recyclerView.setAdapter(articleAdapter);

                    }
                })
        );
    }
}
