package com.example.articlemanagment.api.service;

import com.example.articlemanagment.entity.ArticleRespone;
import com.example.articlemanagment.entity.DATAEntity;

import io.reactivex.Observable;
import retrofit2.http.GET;

public interface ArticleService {

 @GET("v1/api/articles")
 Observable<ArticleRespone> getAllArticles();

}
