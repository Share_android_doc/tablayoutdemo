package com.example.articlemanagment.api.network;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class ServiceGenerator {

    public final static String BASE_URL = "http://api-ams.me/";

    public static Retrofit.Builder builder = new Retrofit.Builder()
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl(BASE_URL);

    public static <S> S createService(Class<S> classService){
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request original = chain.request();
                Request.Builder requestBuilder = original.newBuilder()
                        .addHeader("Authorization","Basic QU1TQVBJQURNSU46QU1TQVBJUEBTU1dPUkQ=")
                        .header("Accept","application/json")
                        .method(original.method(), original.body());
                return chain.proceed(requestBuilder.build());
            }
        });

        Retrofit retrofit = builder.client(httpClient.build()).build();

        return retrofit.create(classService);

    }


}
