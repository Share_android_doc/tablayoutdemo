package com.example.articlemanagment.callBack;

import com.example.articlemanagment.entity.DATAEntity;


public interface ArticleCallBack {
    public void getAllData(DATAEntity article);
}
