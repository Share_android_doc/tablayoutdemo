package com.example.articlemanagment.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.articlemanagment.R;
import com.example.articlemanagment.entity.DATAEntity;

import java.util.List;

public class ArticleAdapter extends RecyclerView.Adapter<ArticleAdapter.MyViewHolder>{

    List<DATAEntity> dataEntityList;
    Context context;
    LayoutInflater inflater;

    public ArticleAdapter(List<DATAEntity> dataEntityList, Context context) {
        this.dataEntityList = dataEntityList;
        this.context = context;
        inflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            View view = inflater.inflate(R.layout.article_lis_layout, null);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int i) {

        DATAEntity dataEntity = dataEntityList.get(i);
        myViewHolder.textView.setText(dataEntity.getTITLE());

    }

    @Override
    public int getItemCount() {
        return dataEntityList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder{

        TextView textView;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            textView = itemView.findViewById(R.id.articleItem);
        }
    }

}
